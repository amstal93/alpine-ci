FROM alpine:3.10

RUN apk update && apk upgrade \
	&& apk add curl git jq zip \
	&& rm -rf /var/cache/apk/*
