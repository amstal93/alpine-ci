![alpine logo](https://alpinelinux.org/alpinelinux-logo.svg)
# alpine-ci
[![pipeline status](https://gitlab.com/kekalainen/alpine-ci/badges/master/pipeline.svg)](https://gitlab.com/kekalainen/alpine-ci/commits/master)

**An [alpine](https://hub.docker.com/_/alpine)-based Docker image for use in CI/CD pipelines.**

## Packages installed
* curl
* git
* jq
* zip